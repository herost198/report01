<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['namespace' => 'Auth'], function () {
    Route::post('/login', 'LoginController@login');
    Route::post('/logout', 'LoginController@logout');
    Route::get('/refresh', 'LoginController@refresh');
    Route::put('/change-password', 'ChangePasswordController@changePassword')->middleware('jwt.auth');
    Route::get('/info-user', 'IndexController@getUser')->middleware('jwt.auth');
});
Route::put('/change-info', 'Users\UpdateController@update')->middleware('jwt.auth');

Route::group(['middleware' => ['jwt.auth', 'userRole:ADMIN'], 'namespace' => 'Users', 'prefix' => 'users'], function () {
    Route::get('/', 'IndexController@index');
    Route::post('/', 'StoreController@store');
    Route::put('/{id}', 'UpdateController@update');
    Route::delete('/{id}', 'DeleteController@destroy');
});
Route::group(['middleware' => 'jwt.auth', 'namespace' => 'Projects', 'prefix' => 'projects'], function () {
    Route::get('/', 'IndexController@index');
    Route::get('/get-all', 'IndexController@getAll');
    Route::post('/', 'StoreController@store')->middleware('userRole:ADMIN,PM');
    Route::get('/{id}', 'ShowController@show');
    Route::put('/{id}', 'UpdateController@update')->middleware('userRole:ADMIN,PM');
    Route::delete('/{id}', 'DestroyController@destroy')->middleware('userRole:ADMIN,PM');

    Route::get('/{projectID}/categories', 'IndexCategoryController@main');
    Route::get('/{projectID}/members', 'IndexMemberController@main');
    Route::get('/{projectID}/flows', 'IndexFlowController@main');
    Route::get('/{projectID}/tasks', 'IndexTaskController@main');
    Route::get('/{projectID}/attributes', 'IndexAttributeController@main');
});
Route::group(['middleware' => ['jwt.auth', 'userRole:PM,ADMIN'], 'namespace' => 'ProjectCategories', 'prefix' => 'project-category'], function () {
    Route::get('/', 'IndexController@index');
    Route::post('/', 'StoreController@store');

    Route::get('/{categoryKey}/{projectId}/edit', 'UpdateController@edit');
    Route::put('/{categoryKey}/{projectId}/', 'UpdateController@update');
    Route::delete('/{categoryKey}/{projectId}', 'DestroyController@destroy');
});
Route::group(['middleware' => ['jwt.auth', 'userRole:PM,ADMIN'], 'namespace' => 'ProjectMembers', 'prefix' => 'project-member'], function () {
    Route::post('/{projectId}', 'StoreController@store');
    Route::put('/{projectId}/{userId}', 'UpdateController@update');
    Route::delete('/{projectId}/{userId}', 'DestroyController@destroy');
});
//['middleware' => ['jwt.auth'],
Route::group(['namespace' => 'Annotations', 'prefix' => 'annotations'], function () {
    Route::get('/projects/{projectID}', 'IndexTasksController@main');
    Route::put('/projects/task/update/{taskID}', 'UpdateTaskController@main');
    Route::get('/projects/task/{taskID}', 'ShowTaskController@main');
});
Route::group(['middleware' => ['jwt.auth', 'userRole:PM,ADMIN'], 'namespace' => 'ProjectAttributes', 'prefix' => 'project-attribute'], function () {
    Route::post('/', 'StoreController@store');
    Route::put('/{attributeKey}/{projectId}/', 'UpdateController@update');
    Route::delete('/{attributeKey}/{projectId}', 'DestroyController@destroy');
});
Route::group([
    'middleware' => ['jwt.auth'],
    'namespace' => 'ReviewCommentations',
    'prefix' => 'review-comment'
], function () {
    Route::post('/', 'StoreController@main');
    Route::get('/{taskID}', 'IndexCommentTaskController@main');
});

Route::group(['middleware' => ['jwt.auth'], 'namespace' => 'Tasks', 'prefix' => 'tasks'], function () {
    Route::group(['middleware' => ['userRole:PM,ADMIN']], function () {
        Route::put('/{projectId}/{taskId}', 'UpdateController@update');
        Route::delete('/{taskId}', 'DestroyController@destroy');
        Route::post('/{projectId}', 'StoreController@store');
    });
    Route::get('/{taskId}/task-status', 'IndexTaskStatusController@main');
    Route::get('/{taskId}/task-status/{taskStatusId}', 'ShowTaskStatusController@main');
    Route::get('/{taskId}/reject-task', 'ChangeTaskController@main');
});

Route::group(['middleware' => ['jwt.auth'], 'namespace' => 'TaskStatus', 'prefix' => 'task-status'], function () {
    Route::get('/{taskStatusId}', 'ShowController@show');
    Route::put('/{taskStatusId}', 'UpdateController@main');
});
