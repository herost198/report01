import VueRouter from 'vue-router';
import Vue from 'vue';
import DashBoard from './views/Dashboard';
import Users from './views/Users';
import Annotation from './views/Annotation';
import Login from './views/Login';
import Profile from './views/Profile';
import Projects from './views/Projects';
import ChangePassword from './components/profile/ChangePasswordComponent.vue';
import UpdateInfo from './components/profile/UpdateInfoComponent.vue';
import ProjectDetail from './views/ProjectDetail';
import TaskDetail from './views/TaskDetail';

const routes = [
  {
    path: '/dashboard',
    component: DashBoard,
    meta: {
      auth: true,
    },
  },
  {
    path: '/',
    component: DashBoard,
    meta: {
      auth: true,
    },
  },
  {
    path: '/users',
    component: Users,
    meta: {
      auth: true,
    },
  },
  {
    path: '/annotation/:projectID/:taskID/:taskStatusID',
    component: Annotation,
    name: 'annotation',
    meta: {
      auth: true,
    },
  },
  {
    path: '/review/:projectID/:taskID/:taskStatusID',
    component: Annotation,
    name: 'review',
    meta: {
      auth: true,
    },
  },
  {
    path: '/login',
    component: Login,
    meta: {
      auth: false,
    },
  },
  {
    path: '/profile',
    component: Profile,
    meta: {
      auth: true,
    },
    children: [
      {
        path: 'info',
        component: UpdateInfo,
        props: {
          user: true,
        },
      },
      {
        path: 'change-password',
        component: ChangePassword,
      },
    ],
  },
  {
    path: '/projects',
    component: Projects,
    meta: {
      auth: true,
    },
  },
  {
    path: '/projects/:id',
    component: ProjectDetail,
    name: 'projectDetail',
    meta: {
      auth: true,
    },
  },
  {
    path: '/projects/:projectID/task-detail/:taskID',
    component: TaskDetail,
    name: 'taskDetail',
    meta: {
      auth: true,
    },
  },
];
Vue.use(VueRouter);
export default new VueRouter({
  base: '/',
  mode: 'history',
  routes,
  linkActiveClass: 'active',
});
