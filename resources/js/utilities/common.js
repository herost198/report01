import moment from 'moment-timezone';

export const MSG_REQUIRED = '{0} không được để trống';
export const MSG_INVALID = '{0} không hợp lệ';
export const MGS_MAX_LENGTH = '{0} không quá {1} ký tự';
export const RECORD_PER_PAGE = 10;
export const ZOOM_IN_SCALE = 1.2;
export const ZOOM_OUT_SCALE = 0.8;
export const POINT_SIZE = 10;
export const STROKE_WIDTH = 2;
export const MAX_CLOSE_SEGMENT = 10;
export const MAX_HISTORY_STEP = 20;
export const WIDTH_CANVAS = 1450;
export const HEIGHT_CANVAS = 730;
export const ANNOTATE_ACTIONS = {
  SELECT: 1,
  DRAW_RECT: 2,
  DRAW_POINT: 3,
  DRAW_CIRCLE: 4,
  DRAW_LINE: 5,
  DRAW_SEGMENT: 6,
  DRAW_SQUARE: 7,
};
export const ANNOTATION_SHAPES = {
  2: 'rect',
  3: 'point',
  4: 'circle',
  5: 'line',
  6: 'segment',
  7: 'square',
};
export const fnCommon = {
  Format() {
    // eslint-disable-next-line prefer-rest-params
    let str = arguments[0];
    for (let i = 0; i < arguments.length - 1; i += 1) {
      // eslint-disable-next-line prefer-rest-params
      str = str.replace(new RegExp(`\\{${i}\\}`, 'gm'), arguments[i + 1]);
    }
    return str;
  },
};
function showNotification(message, type) {
  // eslint-disable-next-line no-undef
  $(document).Toasts('create', {
    class: type,
    title: 'Notification',
    autohide: true,
    delay: 3000,
    body: message,
  });
}
const USER_ROLE = {
  1: 'PM',
  2: 'QA',
  3: 'ANNOTATOR',
  9: 'ADMIN',
};
const USER_ROLE_STRING = {
  PM: 1,
  QA: 2,
  ANNOTATOR: 3,
  ADMIN: 9,
};
const USER_STATUS = [
  {
    id: 9,
    status: 'InActive',
    style: 'bg-danger',
  },
  {
    id: 1,
    status: 'Active',
    style: 'bg-success',
  },
];
const IS_ACTIVE = 2;
const HISTORY_ACTIONS = {
  DELETE_RECT: 1,
  DELETE_POINT: 2,
  DELETE_CIRCLE: 3,
  DELETE_LINE: 4,
  DELETE_SEGMENT: 5,
  DRAW_RECT: 6,
  DRAW_POINT: 7,
  DRAW_CIRCLE: 8,
  DRAW_LINE: 9,
  DRAW_SEGMENT: 10,
  DRAG_RECT: 11,
  DRAG_POINT: 12,
  DRAG_CIRCLE: 13,
  DRAG_LINE: 14,
  DRAG_SEGMENT: 15,
};
const WORKING_TYPE = {
  ANNOTATING: 1,
  REVIEWING: 2,
};
const WORKING_TYPE_STRING = {
  annotation: 'ANNOTATING',
  review: 'REVIEWING',
};
const STATUS_TASK_STATUS = {
  OPEN: 1,
  INPROGESS: 2,
  CLOSE: 3,
};
const TASK_STATUS_STRING = {
  0: 'Unassign',
  1: 'Open',
  2: 'Annotating',
  3: 'Fixing',
  4: 'Review 1',
  5: 'Review 2',
  6: 'Review 3',
  9: 'Finish',
};
function getUserRole(role) {
  return USER_ROLE[role];
}
function getListUserRole() {
  return USER_ROLE;
}

function getUserStatus(status) {
  return USER_STATUS.find((value) => value.id === status);
}
function getListUserStatus() {
  return USER_STATUS;
}
function checkValueBetween(val, min, max) {
  return val >= min && val <= max;
}

const PROJECT_STATUS = [
  {
    id: 1,
    status: 'Open',
    style: 'bg-secondary',
  },
  {
    id: 2,
    status: 'Doing',
    style: 'bg-info',
  },
  {
    id: 3,
    status: 'Done',
    style: 'bg-success',
  },
];
const TASK_STATUS_DEFAULT = {
  UNASSIGN: 0,
  OPEN: 1,
  ANNOTATING: 2,
  FIXING: 3,
  REVIEW_FIRST: 4,
  REVIEW_SECOND: 5,
  REVIEW_THIRD: 6,
  FINISH: 9,
};
function getProjectStatus(status) {
  return PROJECT_STATUS.find((value) => value.id === status);
}
function getListProjectStatus() {
  return PROJECT_STATUS;
}
const FORMAT_DATE = 'YYYY-MM-DD';

function formatDate(date) {
  return moment(date).format(FORMAT_DATE);
}
function isActive(status) {
  return status === IS_ACTIVE;
}
const CSS_COLOR_NAMES = [
  'AliceBlue',
  'AntiqueWhite',
  'Aqua',
  'Aquamarine',
  'Azure',
  'Beige',
  'Bisque',
  'Black',
  'BlanchedAlmond',
  'Blue',
  'BlueViolet',
  'Brown',
  'BurlyWood',
  'CadetBlue',
  'Chartreuse',
  'Chocolate',
  'Coral',
  'CornflowerBlue',
  'Cornsilk',
  'Crimson',
  'Cyan',
  'DarkBlue',
  'DarkCyan',
  'DarkGoldenRod',
  'DarkGray',
  'DarkGrey',
  'DarkGreen',
  'DarkKhaki',
  'DarkMagenta',
  'DarkOliveGreen',
  'DarkOrange',
  'DarkOrchid',
  'DarkRed',
  'DarkSalmon',
  'DarkSeaGreen',
  'DarkSlateBlue',
  'DarkSlateGray',
  'DarkSlateGrey',
  'DarkTurquoise',
  'DarkViolet',
  'DeepPink',
  'DeepSkyBlue',
  'DimGray',
  'DimGrey',
  'DodgerBlue',
  'FireBrick',
  'FloralWhite',
  'ForestGreen',
  'Fuchsia',
  'Gainsboro',
  'GhostWhite',
  'Gold',
  'GoldenRod',
  'Gray',
  'Grey',
  'Green',
  'GreenYellow',
  'HoneyDew',
  'HotPink',
  'IndianRed',
  'Indigo',
  'Ivory',
  'Khaki',
  'Lavender',
  'LavenderBlush',
  'LawnGreen',
  'LemonChiffon',
  'LightBlue',
  'LightCoral',
  'LightCyan',
  'LightGoldenRodYellow',
  'LightGray',
  'LightGrey',
  'LightGreen',
  'LightPink',
  'LightSalmon',
  'LightSeaGreen',
  'LightSkyBlue',
  'LightSlateGray',
  'LightSlateGrey',
  'LightSteelBlue',
  'LightYellow',
  'Lime',
  'LimeGreen',
  'Linen',
  'Magenta',
  'Maroon',
  'MediumAquaMarine',
  'MediumBlue',
  'MediumOrchid',
  'MediumPurple',
  'MediumSeaGreen',
  'MediumSlateBlue',
  'MediumSpringGreen',
  'MediumTurquoise',
  'MediumVioletRed',
  'MidnightBlue',
  'MintCream',
  'MistyRose',
  'Moccasin',
  'NavajoWhite',
  'Navy',
  'OldLace',
  'Olive',
  'OliveDrab',
  'Orange',
  'OrangeRed',
  'Orchid',
  'PaleGoldenRod',
  'PaleGreen',
  'PaleTurquoise',
  'PaleVioletRed',
  'PapayaWhip',
  'PeachPuff',
  'Peru',
  'Pink',
  'Plum',
  'PowderBlue',
  'Purple',
  'RebeccaPurple',
  'Red',
  'RosyBrown',
  'RoyalBlue',
  'SaddleBrown',
  'Salmon',
  'SandyBrown',
  'SeaGreen',
  'SeaShell',
  'Sienna',
  'Silver',
  'SkyBlue',
  'SlateBlue',
  'SlateGray',
  'SlateGrey',
  'Snow',
  'SpringGreen',
  'SteelBlue',
  'Tan',
  'Teal',
  'Thistle',
  'Tomato',
  'Turquoise',
  'Violet',
  'Wheat',
  'White',
  'WhiteSmoke',
  'Yellow',
  'YellowGreen',
];
export const STATUS_OS_TASK_STATUS = {
  1: 'Open',
  2: 'Inprogess',
  3: 'Close',
};
export const STRING_STATUS_OS_TASK_STATUS = {
  OPEN: 1,
  INPROGESS: 2,
  CLOSE: 3,
};
export const TASK_STATUS = [
  {
    id: 0,
    status: 'Unassign',
    style: 'bg-secondary',
  },
  {
    id: 1,
    status: 'Open',
    style: 'bg-primary',
  },
  {
    id: 2,
    status: 'Annotating',
    style: 'bg-info',
  },
  {
    id: 3,
    status: 'Fixing',
    style: 'bg-warning',
  },
];
function getSecondsTimeWorking(timeString) {
  if (timeString === null) return 0;
  const arrayTime = timeString.split(':');
  if (arrayTime.length < 3) return 0;
  const timeSeconds = parseInt(arrayTime[0], 10) * 3600 + parseInt(arrayTime[1], 10) * 60 + parseInt(arrayTime[2], 10);
  return timeSeconds;
}
function sumTotalTime(task) {
  const taskStatuses = [...task.task_status_with_user];
  let totalTime = 0;
  if (taskStatuses.length === 0) return 0;
  taskStatuses.forEach((taskStatus) => {
    const taskStatusTmp = taskStatus;
    if (taskStatusTmp.time_working === null || taskStatusTmp.time_working === 0) {
      taskStatusTmp.time_working = '00:00:00';
      return;
    }
    taskStatusTmp.time_working += '';
    totalTime += getSecondsTimeWorking(taskStatusTmp.time_working);
  });
  return totalTime;
}
function getUserByStatus(task) {
  const infos = task.task_status_with_user;
  const users = infos.filter((info) => info.user != null);
  return users[0] === undefined ? '' : users[0].user.full_name;
}
export {
  sumTotalTime,
  getUserByStatus,
  isActive,
  USER_ROLE,
  USER_ROLE_STRING,
  HISTORY_ACTIONS,
  WORKING_TYPE,
  WORKING_TYPE_STRING,
  getListUserRole,
  getUserRole,
  getListUserStatus,
  getUserStatus,
  showNotification,
  getProjectStatus,
  getListProjectStatus,
  getSecondsTimeWorking,
  checkValueBetween,
  formatDate,
  CSS_COLOR_NAMES,
  STATUS_TASK_STATUS,
  TASK_STATUS_STRING,
  TASK_STATUS_DEFAULT,
  PROJECT_STATUS,
};
