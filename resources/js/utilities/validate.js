export function validateEmail(email) {
  if (email === '') {
    return false;
  }
  // eslint-disable-next-line no-useless-escape
  const regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regEmail.test(email);
}
export function validatePassword(password) {
  return !(password === '' || password.length < 8);
}
export const regexAlpha = /^[a-zA-Z0-9_.-]*$/;
export const messageValidate = {
  required: 'Field is required.',
  alpha: 'The input field is alpha',
  maxLength: 'Input length must not exceed 127 characters',
};
