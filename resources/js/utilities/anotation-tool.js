export function getRelativePointerPosition(node) {
  // the function will return pointer position relative to the passed node
  const transform = node.getAbsoluteTransform().copy();
  // to detect relative position we need to invert transform
  transform.invert();

  // get pointer (say mouse or touch) position
  const pos = node.getStage().getPointerPosition();

  // now we find relative point
  return transform.point(pos);
}
export function limitAttributes(stage, newAttrs) {
  const box = stage.findOne('#image-selected').getClientRect();
  const minX = -box.width + stage.width() / 2;
  const maxX = stage.width() / 2;

  const x = Math.max(minX, Math.min(newAttrs.x, maxX));

  const minY = -box.height + stage.height() / 2;
  const maxY = stage.height() / 2;

  const y = Math.max(minY, Math.min(newAttrs.y, maxY));

  const scale = Math.max(0.05, newAttrs.scale);

  return { x, y, scale };
}
// reverse co-ords if user drags left / up
export function reverseRect(r1, r2) {
  let r1x = r1.x; let r1y = r1.y; let r2x = r2.x; let r2y = r2.y; let
    d;
  if (r1x > r2x) {
    d = Math.abs(r1x - r2x);
    r1x = r2x;
    r2x = r1x + d;
  }
  if (r1y > r2y) {
    d = Math.abs(r1y - r2y);
    r1y = r2y;
    r2y = r1y + d;
  }
  return ({
    x1: r1x, y1: r1y, x2: r2x, y2: r2y,
  }); // return the corrected rect.
}
