import { MAX_HISTORY_STEP } from '../../utilities/common';

/* eslint-disable no-param-reassign */
/* eslint-disable no-shadow */
const state = {
  images: [],
  imgChosen: null,
  labels: [],
  selected: [],
  scale: 1,
  history: [],
  historyStep: 0,
  currentUser: null,
  currentTaskStatus: {},
};

const mutations = {
  SET_INIT_STATE: (state) => {
    state.images = [];
    state.imageChosen = null;
    state.labels = [];
    state.historyStep = 0;
    state.history = [];
  },
  SET_IMAGES: (state, images) => {
    state.images = images;
  },
  SET_IMAGE_CHOSEN: (state, imgChosen) => {
    state.imgChosen = imgChosen;
  },
  SET_LABELS: (state, labels) => {
    const labelsTmp = Object.keys(labels).map((key) => ({ ...labels[key], edit: 'edit' in labels[key] ? labels[key].edit : false }));
    state.labels = labelsTmp;
    state.imgChosen.labels = labelsTmp;
    const imagesTmp = state.images.map((image) => {
      if (image.id === state.imgChosen.id) {
        return state.imgChosen;
      }
      return image;
    });
    state.images = imagesTmp;
  },
  SET_SELECTED: (state, selected) => {
    state.selected = selected;
  },
  SET_SCALE: (state, scale) => {
    state.scale = scale;
  },
  SET_HISTORY: (state, history) => {
    state.history = history;
  },
  ADD_HISTORY: (state, historyItem) => {
    let historyTmp = [...state.history];
    if (state.historyStep > MAX_HISTORY_STEP) {
      state.historyStep -= 1;
      historyTmp = historyTmp.slice(1, state.historyStep + 1);
    } else {
      historyTmp = historyTmp.slice(0, state.historyStep + 1);
    }
    historyTmp.push(historyItem);
    state.history = historyTmp;
  },
  SET_HISTORY_STEP: (state, historyStep) => {
    state.historyStep = historyStep;
  },
  INCREASE_HISTORY_STEP: (state) => {
    state.historyStep += 1;
  },
  REDUCE_HISTORY_STEP: (state) => {
    state.historyStep -= 1;
  },
  SET_CURRENT_USER: (state, currentUser) => {
    state.currentUser = currentUser;
  },
  SET_TASK_STATUS: (state, taskStatus) => {
    state.currentTaskStatus = taskStatus;
  },
};

const actions = {
  setInitState({ commit }) {
    commit('SET_INIT_STATE');
  },
  setImages({ commit }, images) {
    commit('SET_IMAGES', images);
  },
  setImgChosen({ commit }, imgChosen) {
    commit('SET_IMAGE_CHOSEN', imgChosen);
  },
  setLabels({ commit }, labels) {
    commit('SET_LABELS', labels);
  },
  setSelected({ commit }, selected) {
    commit('SET_SELECTED', selected);
  },
  setScale({ commit }, scale) {
    commit('SET_SCALE', scale);
  },
  setHistory({ commit }, history) {
    commit('SET_HISTORY', history);
  },
  addHistory({ commit }, historyItem) {
    commit('ADD_HISTORY', historyItem);
  },
  setHistoryStep({ commit }, historyStep) {
    commit('SET_HISTORY_STEP', historyStep);
  },
  increaseHistoryStep({ commit }) {
    commit('INCREASE_HISTORY_STEP');
  },
  reduceHistoryStep({ commit }) {
    commit('REDUCE_HISTORY_STEP');
  },
  setCurrentUser({ commit }, currentUser) {
    commit('SET_CURRENT_USER', currentUser);
  },
  setTaskStatus({ commit }, taskStatus) {
    commit('SET_TASK_STATUS', taskStatus);
  },
};
const getters = {
  images: (state) => state.images,
  labels: (state) => state.labels,
  imageChosen: (state) => state.imgChosen,
  labelsSelected: (state) => state.labels.filter((label) => (state.selected.includes(label.type))),
  labelsByType: (state, getters) => (type) => {
    const labelsEnable = getters.labelsSelected;
    return labelsEnable.filter((label) => (label.type === type && label.isShowing));
  },
  history: (state) => state.history,
  historyStep: (state) => state.historyStep,
  currentUser: (state) => state.currentUser,
  currentTaskStatus: (state) => state.currentTaskStatus,
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
