// Add the ES2015 formation
import axios from 'axios';

window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
  // eslint-disable-next-line global-require
  window.Popper = require('popper.js').default;
  // eslint-disable-next-line global-require
  window.jQuery = require('jquery');
  // eslint-disable-next-line global-require
  window.$ = require('jquery');

  // eslint-disable-next-line global-require
  require('bootstrap');
  // eslint-disable-next-line global-require
  require('admin-lte');
  // eslint-disable-next-line no-empty
} catch (e) { }

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = axios;

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
