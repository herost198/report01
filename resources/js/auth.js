import bearer from '@websanova/vue-auth/drivers/auth/bearer';
import axios from '@websanova/vue-auth/drivers/http/axios.1.x';
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x';
// Auth base configuration some of this options
// can be override in method calls
const config = {
  auth: bearer,
  http: axios,
  router,
  tokenDefaultName: 'token',
  tokenStore: ['localStorage'],
  rolesVar: 'role',
  authRedirect: { path: '/login' },
  notFoundRedirect: '/',
  registerData: {
    url: '/api/register', method: 'POST', redirect: '/login',
  },
  loginData: {
    url: '/api/login', method: 'POST', redirect: '/', fetchUser: false,
  },
  logoutData: {
    url: '/api/logout', method: 'POST', redirect: '/login', makeRequest: true,
  },
  fetchData: {
    url: '/api/info-user', method: 'GET', enabled: true,
  },
  refreshData: {
    url: '/api/refresh', method: 'GET', enabled: true, interval: 30,
  },
};
export default config;
