const limitAttributes = (stage, newAttrs) => {
  const box = stage.findOne('#image-selected').getClientRect();
  const minX = -box.width + stage.width() / 2;
  const maxX = stage.width() / 2;

  const x = Math.max(minX, Math.min(newAttrs.x, maxX));

  const minY = -box.height + stage.height() / 2;
  const maxY = stage.height() / 2;

  const y = Math.max(minY, Math.min(newAttrs.y, maxY));

  const scale = Math.max(0.05, newAttrs.scale);

  return { x, y, scale };
};

export const getStageZoom = (stage, newScale) => {
  const oldScale = stage.scaleX();

  const pos = {
    x: stage.width() / 2,
    y: stage.height() / 2,
  };
  const mousePointTo = {
    x: pos.x / oldScale - stage.x() / oldScale,
    y: pos.y / oldScale - stage.y() / oldScale,
  };

  const newPos = {
    x: -(mousePointTo.x - pos.x / newScale) * newScale,
    y: -(mousePointTo.y - pos.y / newScale) * newScale,
  };
  const newAttrs = limitAttributes(stage, { ...newPos, scale: newScale });

  stage.to({
    x: newAttrs.x,
    y: newAttrs.y,
    scaleX: newAttrs.scale,
    scaleY: newAttrs.scale,
    duration: 0.1,
  });
  return stage;
};
export function getShapeFromLabels(shapeLabels) {
  return shapeLabels.map((label) => {
    const shape = { ...label.shape };
    shape.visible = label.isShowing;
    return shape;
  });
}
export function getLabelTransformerEnd(labels, shape, event) {
  const labelResults = [...labels];
  labelResults.forEach((label, index) => {
    if (label.shape.name === shape.name) {
      labelResults[index].shape.x = event.target.x();
      labelResults[index].shape.y = event.target.y();
      labelResults[index].shape.scaleX = event.target.scaleX();
      labelResults[index].shape.scaleY = event.target.scaleY();
      labelResults[index].shape.name = `${label.shape.type}-${event.target.x()}-${event.target.y()}`;
    }
  });
  return labelResults;
}
