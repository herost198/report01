/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import VueAxios from 'vue-axios';
import VueAuth from '@websanova/vue-auth';
import Vuelidate from 'vuelidate';
import VueKonva from 'vue-konva';
import DatePicker from 'vue2-datepicker';
import ToggleButton from 'vue-js-toggle-button';
import App from './App.vue';
import router from './routes';
import auth from './auth';
import store from './store';

require('./bootstrap');

window.Vue = Vue;
// Set Vue router
Vue.router = router;
// eslint-disable-next-line no-undef
Vue.use(VueAxios, axios);
Vue.use(VueAuth, auth);
Vue.use(Vuelidate);
Vue.use(VueKonva);
Vue.use(Vuelidate);
Vue.use(DatePicker);
Vue.use(ToggleButton);
Vue.use(require('vue-moment'));

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  router,
  store,
  components: {
    app: App,
  },
});
