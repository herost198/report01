## HBLab Annotate Tool

Phát triển hệ thống hỗ trợ quản lý và thực hiện công việc cho mảng annotation

## Setup project
```
docker-compose up -d
docker exec -it report01_php bash (Nếu lỗi ở windows có thể phải thêm winpty vào đầu)

cp .env.example .env
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan jwt:secret
php artisan db:seed
composer dump-autoload
php artisan apidoc:generate
```
