<?php

namespace App\Services;

use App\Enums\Active;
use App\Enums\UserRole;
use App\Repositories\TaskRepository;
use Illuminate\Support\Facades\Auth;

class TaskService
{
    private $taskRepo;

    public function __construct(TaskRepository $taskRepo)
    {
        $this->taskRepo = $taskRepo;
    }

    public function findByProjectID($projectID)
    {
        $currentUser = Auth::user();
        if ($currentUser->role == UserRole::PM || $currentUser->role == UserRole::ADMIN) {
            return $this->taskRepo->makeModel()
                ->where('project_id', $projectID)
                ->where('task.is_active', Active::YES)
                ->select('task.*')
                ->with('taskStatusWithUser')
                ->paginate(config('constants.record_per_page'))
                ->toArray();
        }
        return $this->taskRepo->makeModel()
            ->where('project_id', $projectID)
            ->where('task.is_active', Active::YES)
            ->join('task_status', 'task.id', '=', 'task_status.task_id')
            ->where('task_status.is_active', '=', true)
            ->where('user_id', '=', $currentUser->id)
            ->select('task.*')
            ->with('taskStatusWithUser')
            ->paginate(config('constants.record_per_page'))
            ->toArray();
    }

    public function updateByID($taskID, $data)
    {
        return $this->taskRepo->update($data, $taskID);
    }

    public function getByID($taskID)
    {
        return $this->taskRepo->makeModel()->find($taskID);
    }

    public function store($data)
    {
        return $this->taskRepo->makeModel()->create($data);
    }

    public function deleteById($taskId)
    {
        return $this->taskRepo->makeModel()->find($taskId)->delete();
    }
}
