<?php

namespace App\Services;

use App\Repositories\TaskStatusRepository;

class TaskStatusService
{
    private $taskStatusRepo;

    public function __construct(TaskStatusRepository $taskStatusRepo)
    {
        $this->taskStatusRepo = $taskStatusRepo;
    }

    public function updateFirstRecordByTaskID($taskID, $data)
    {
        return $this->taskStatusRepo->makeModel()
            ->where('task_id', $taskID)
            ->orderBy('created_at')
            ->first()->update($data);
    }

    public function store($data)
    {
        return $this->taskStatusRepo->makeModel()->create($data);
    }

    public function findByTaskID($taskId)
    {
        return $this->taskStatusRepo->makeModel()->where('task_id', $taskId)->get();
    }

    public function getByTaskID($taskId)
    {
        return $this->taskStatusRepo->makeModel()->where('task_id', $taskId)->with('user:id,full_name,email')->get();
    }

    public function deleteByTaskId($taskId)
    {
        return $this->taskStatusRepo->makeModel()->where('task_id', $taskId)->delete();
    }

    public function update($id, array $data)
    {
        return $this->taskStatusRepo->update($data, $id);
    }

    public function updateByID($taskStatusId, $data)
    {
        return $this->taskStatusRepo->makeModel()->find($taskStatusId)->update($data);
    }

    public function firstByTaskId($taskId)
    {
        return $this->taskStatusRepo->makeModel()->where('task_id', $taskId)->first();
    }

    public function getByID($taskStatusID)
    {
        return $this->taskStatusRepo->makeModel()->find($taskStatusID);
    }

    public function getListUserIDByTaskID($taskID)
    {
        return $this->taskStatusRepo->makeModel()->where('task_id', $taskID)->pluck('user_id')->toArray();
    }

    public function getHasActiveByTaskID($taskID)
    {
        return $this->taskStatusRepo->makeModel()
            ->where('task_id', $taskID)
            ->where('is_active', 1)->get();
    }
}
