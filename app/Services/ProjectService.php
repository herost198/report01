<?php

namespace App\Services;

use App\Enums\ProjectStatus;
use App\Repositories\ProjectRepository;

class ProjectService
{
    /**
     * @var UserRepository
     */
    protected $projectRepo;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $projectRepo
     */
    public function __construct(ProjectRepository $projectRepo)
    {
        $this->projectRepo = $projectRepo;
    }

    /**
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function all($isPagination = true, $paramsSearch = [])
    {
        if (!$isPagination) {
            return $this->projectRepo->makeModel()->get();
        }
        return $this->projectRepo->makeModel()
            ->when($paramsSearch['search'], function ($query) use ($paramsSearch) {
                return $query->where('name', 'like', '%' . $paramsSearch['search'] . '%')
                    ->orWhere('key', 'like', '%' . $paramsSearch['search'] . '%');
            })
            ->when($paramsSearch['statuses'], function ($query) use ($paramsSearch) {
                return $query->whereIn('status', $paramsSearch['statuses']);
            })
            ->paginate(config('constants.record_per_page'));
    }

    /**
     * @param array $data
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function store(array $data)
    {
        return $this->projectRepo->makeModel()->create($data);
    }

    public function update($id, array $data)
    {
        $update = $this->projectRepo->update($data, $id);
        if ($update) {
            return $update;
        }
        return false;
    }

    public function delete($id)
    {
        return $this->projectRepo->delete($id);
    }

    public function checkStatusProject($id)
    {
        $project = $this->projectRepo->find($id);
        return $project->status == ProjectStatus::OPEN;
    }

    public function getProjectByProjectIds($projectIds, $paramsSearch = [])
    {
        return $this->projectRepo->makeModel()->WhereIn('id', $projectIds)
            ->when($paramsSearch['search'], function ($query) use ($paramsSearch) {
                return $query->where('name', 'like', '%' . $paramsSearch['search'] . '%')
                    ->orWhere('key', 'like', '%' . $paramsSearch['search'] . '%');
            })
            ->when($paramsSearch['statuses'], function ($query) use ($paramsSearch) {
                return $query->whereIn('status', $paramsSearch['statuses']);
            })
            ->paginate(config('constants.record_per_page'));
    }

    public function getProjectByProjectId($projectId)
    {
        return $this->projectRepo->makeModel()->where('id', $projectId)->with('flow')
            ->first();
    }

    public function findByID($projectId)
    {
        return $this->projectRepo->makeModel()->find($projectId);
    }
}
