<?php

namespace App\Services;

use App\Enums\UserRole;
use App\Repositories\ProjectMemberRepository;
use Illuminate\Support\Facades\Auth;

class ProjectMemberService
{
    /***
     * @var ProjectMemberRepository
     */
    protected $projectMemberRepo;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $projectRepo
     */
    public function __construct(ProjectMemberRepository $projectMemberRepo)
    {
        $this->projectMemberRepo = $projectMemberRepo;
    }

    /**
     * @param $userId
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getProjectMemberByUserIds($userId)
    {
        return $this->projectMemberRepo->makeModel()->where('user_id', $userId)->get();
    }

    public function deleteProjectMember($projectId)
    {
        return $this->projectMemberRepo->deleteWhere(['project_id' => $projectId]);
    }

    public function hasPermissionByProjectId($projectId)
    {
        $user = Auth::user();
        if ($user->role == UserRole::ADMIN) {
            return true;
        }
        $listProjectsOfUser = $this->getProjectMemberByUserIds($user->id);
        $arrListProjectsIdOfUser = $listProjectsOfUser->pluck('project_id')->toArray();
        return in_array($projectId, $arrListProjectsIdOfUser);
    }

    public function getByProjectId($projectId)
    {
        return $this->projectMemberRepo->makeModel()
            ->where('project_id', $projectId)->with('user')
            ->get();
    }

    public function store($data)
    {
        return $this->projectMemberRepo->makeModel()
            ->create($data);
    }

    public function updateProMemberByProIdAndUserId($projectId, $userId, $data)
    {
        return $this->projectMemberRepo->makeModel()
            ->where('user_id', $userId)
            ->where('project_id', $projectId)
            ->update($data);
    }

    public function destroyByProIdAndUserId($pojectId, $userId)
    {
        return $this->projectMemberRepo->makeModel()
            ->where('project_id', $pojectId)
            ->where('user_id', $userId)
            ->delete();
    }

    public function getArrayUserIdByProjectId($projectId)
    {
        return $this->projectMemberRepo->makeModel()
            ->where('project_id', $projectId)
            ->get()->pluck('user_id')
            ->toArray();
    }
}
