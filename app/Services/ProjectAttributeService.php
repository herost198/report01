<?php

namespace App\Services;

use App\Repositories\ProjectAttributeRepository;

class ProjectAttributeService
{
    protected $projectAttributeRepository;


    public function __construct(ProjectAttributeRepository $projectAttributeRepository)
    {
        $this->projectAttributeRepository = $projectAttributeRepository;
    }

    public function all()
    {
        return $this->projectCategoryRepository->makeModel()
            ->with('project:id,name')
            ->paginate(config('constants.record_per_page'), ['project_id', 'category_key', 'category_name',]);
    }

    public function getByProjectId($projectId)
    {
        return $this->projectAttributeRepository->makeModel()
            ->where('project_id', $projectId)
            ->with('project')
            ->get();
    }

    public function updateProjectAttribute($attributeKey, $projectId, array $data)
    {
        $isUpdate = $this->projectAttributeRepository->makeModel()
            ->where('project_id', $projectId)
            ->where('attribute_key', $attributeKey)
            ->update($data);
        if ($isUpdate) {
            $attributeKeyChange = $attributeKey;
            if (!empty($data['attribute_key'])) {
                $attributeKeyChange = $data['attribute_key'];
            }
            return $this->projectAttributeRepository->makeModel()
                ->where('project_id', $projectId)
                ->where('attribute_key', $attributeKeyChange)
                ->first(['project_id', 'attribute_key', 'attribute_name']);
        }
        return [];
    }

    public function destroyByAttrKeyAndProId($attributeKey, $projectId)
    {
        return $this->projectAttributeRepository->makeModel()
            ->where('project_id', $projectId)
            ->where('attribute_key', $attributeKey)
            ->delete();
    }

    public function store($data)
    {
        return $this->projectAttributeRepository->makeModel()->create($data);
    }

    public function destroyByProjectId($projectId)
    {
        return $this->projectAttributeRepository->makeModel()
            ->where('project_id', $projectId)
            ->delete();
    }
}
