<?php

namespace App\Services;

use App\Repositories\ProjectCategoryRepository;

class ProjectCategoryService
{
    /***
     * @var ProjectCategoryRepository
     */
    protected $projectCategoryRepository;

    /***
     * ProjectCategoryService constructor.
     * @param ProjectCategoryRepository $projectCategoryRepository
     */
    public function __construct(ProjectCategoryRepository $projectCategoryRepository)
    {
        $this->projectCategoryRepository = $projectCategoryRepository;
    }

    /***
     * @param $id
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function find($id)
    {
        return $this->projectCategoryRepository->makeModel()->find($id);
    }

    /***
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function all()
    {
        return $this->projectCategoryRepository->makeModel()
            ->with('project:id,name')
            ->paginate(config('constants.record_per_page'), ['project_id', 'category_key', 'category_name',]);
    }

    public function store($data)
    {
        return $this->projectCategoryRepository->makeModel()->create($data);
    }

    /***
     * @param $id
     * @param array $data
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function updateProjectCategory($categoryKey, $projectId, array $data)
    {
        $isUpdate = $this->projectCategoryRepository->makeModel()
            ->where('project_id', $projectId)
            ->where('category_key', $categoryKey)
            ->update($data);
        if ($isUpdate) {
            $categoryKeyChange = $categoryKey;
            if (!empty($data['category_key'])) {
                $categoryKeyChange = $data['category_key'];
            }
            return $this->projectCategoryRepository->makeModel()
                ->where('project_id', $projectId)
                ->where('category_key', $categoryKeyChange)
                ->first(['project_id', 'category_key', 'category_name']);
        }
        return [];
    }

    /***
     * @param $id
     * @return mixed
     */
    public function destroyByCatKeyAndProId($categoryKey, $projectId)
    {
        return $this->projectCategoryRepository->makeModel()
            ->where('project_id', $projectId)
            ->where('category_key', $categoryKey)
            ->delete();
    }

    public function destroyByProjectId($projectId)
    {
        return $this->projectCategoryRepository->makeModel()
            ->where('project_id', $projectId)
            ->delete();
    }

    /***
     * @param $projectIds
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getProjectCategoryByProjectIds($projectIds)
    {
        return $this->projectCategoryRepository->makeModel()->whereIn('project_id', $projectIds)
            ->with('project')
            ->paginate(config('constants.record_per_page'));
    }

    public function getProjectCategory($categoryKey, $projectId)
    {
        return $this->projectCategoryRepository->makeModel()
            ->where('project_id', $projectId)
            ->where('category_key', $categoryKey)->first(['project_id', 'category_key', 'category_name']);
    }

    public function getByProjectId($projectId)
    {
        return $this->projectCategoryRepository->makeModel()
            ->where('project_id', $projectId)
            ->with('project')
            ->get();
    }
}
