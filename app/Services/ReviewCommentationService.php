<?php

namespace App\Services;

use App\Repositories\ReviewCommentationRepository;

class ReviewCommentationService
{
    protected $reviewCmtRepo;

    public function __construct(ReviewCommentationRepository $reviewCmtRepo)
    {
        $this->reviewCmtRepo = $reviewCmtRepo;
    }

    public function store(array $data)
    {
        return $this->reviewCmtRepo->makeModel()->create($data);
    }

    public function findByTaskID($taskID)
    {
        return $this->reviewCmtRepo->makeModel()
            ->where('task_id', $taskID)
            ->orderBy('created_at')
            ->with('user')
            ->get();
    }
    public function update($id, array $data)
    {
        $updateReviewCmt = $this->reviewCmtRepo->update($data, $id);
        if ($updateReviewCmt) {
            return $updateReviewCmt;
        }
        return false;
    }
}
