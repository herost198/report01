<?php

namespace App\Services;

use App\Repositories\FlowRepository;

class FlowService
{
    protected $flowRepo;

    public function __construct(FlowRepository $flowRepo)
    {
        $this->flowRepo = $flowRepo;
    }

    public function store(array $data)
    {
        return $this->flowRepo->makeModel()->create($data);
    }

    public function getByProjectID($projectID)
    {
        return $this->flowRepo->makeModel()
            ->where('project_id', $projectID)
            ->orderBy('order')
            ->get();
    }

    public function destroyByProjectId($projectID)
    {
        return $this->flowRepo->makeModel()
            ->where('project_id', $projectID)
            ->delete();
    }

    public function findFlowByProjectID($projectID)
    {
        return $this->flowRepo->makeModel()
            ->where('project_id', $projectID)
            ->get();
    }
}
