<?php

namespace App\Services;

use App\Enums\UserStatus;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * AuthService constructor.
     * @param UserRepository $userRepo
     */
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function all($search = null, $isPagination = true)
    {
        if (!$isPagination) {
            return $this->userRepo->makeModel()
                ->where('status', UserStatus::ACTIVE)->get();
        }
        return $this->userRepo->makeModel()
            ->where('status', UserStatus::ACTIVE)
            ->when($search, function ($query) use ($search) {
                return $query
                    ->where('full_name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%');
            })
            ->paginate(config('constants.record_per_page'));
    }

    public function store($data)
    {
        return $this->userRepo->makeModel()->create($data);
    }

    public function updateUser($id, array $data)
    {
        $update = $this->userRepo->update($data, $id);
        if ($update) {
            return $update;
        }
        return false;
    }

    public function checkPassword($password, $id)
    {
        $user = $this->userRepo->find($id);
        if ($user != null && Hash::check($password, $user->password)) {
            return true;
        }
        return false;
    }
}
