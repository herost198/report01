<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class ProjectAttributeRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'App\Models\ProjectAttribute';
    }
}
