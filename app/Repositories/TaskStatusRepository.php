<?php

namespace App\Repositories;

use App\Models\TaskStatus;
use Prettus\Repository\Eloquent\BaseRepository;

class TaskStatusRepository extends BaseRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaskStatus::class;
    }
}
