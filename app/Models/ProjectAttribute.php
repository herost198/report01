<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAttribute extends Model
{
    protected $table = 'project_attribute';
    protected $primaryKey = ['project_id', 'attribute_key'];
    public $incrementing = false;

    protected $fillable = [
        'project_id', 'attribute_key', 'attribute_name'
    ];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
