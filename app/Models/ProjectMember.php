<?php

namespace App\Models;

use App\Enums\Active;
use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model
{
    protected $table = 'project_member';
    protected $attributes = [
        'is_active' => Active::YES,
    ];
    protected $primaryKey = ['user_id', 'project_id'];
    protected $fillable = [
        'project_id', 'user_id', 'is_active'
    ];
    public $incrementing = false;

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
