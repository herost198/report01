<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';
    protected $fillable = ['name', 'key', 'start_at', 'end_at', 'note', 'status'];

    public function flow()
    {
        return $this->hasMany(Flow::class, 'project_id', 'id');
    }
}
