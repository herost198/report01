<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $table = 'project_category';
    protected $primaryKey = ['project_id', 'category_key'];
    public $incrementing = false;

    protected $fillable = [
        'project_id', 'category_key', 'category_name'
    ];

    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
