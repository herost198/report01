<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    protected $table = 'user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected const DEFAULT_USER_STATUS = 1;
    protected $fillable = [
        'email', 'password', 'full_name', 'role', 'status', 'phone'
    ];
    protected $attributes = [
        'status' => self::DEFAULT_USER_STATUS
    ];
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
