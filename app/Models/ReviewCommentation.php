<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewCommentation extends Model
{
    protected $table = 'review_cmt';

    protected $fillable = [
      'id', 'user_id', 'task_id', 'comment', 'created_at', 'updated_at'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
