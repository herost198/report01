<?php

namespace App\Models;

use App\Enums\StatusTask;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task';

    protected $fillable = [
        'project_id', 'title', 'src_url', 'output_json', 'is_active', 'status'
    ];

    protected $attributes = [
        'status' => StatusTask::UNASSIGN,
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function taskStatusWithUser()
    {
        return $this->hasMany(TaskStatus::class)->with('user');
    }
}
