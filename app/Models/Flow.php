<?php

namespace App\Models;

use App\Enums\Active;
use Illuminate\Database\Eloquent\Model;

class Flow extends Model
{
    protected $table = 'flow';
    protected $fillable = ['step_name', 'order', 'is_active', 'project_id', 'user_id'];
    protected $attributes = [
        'is_active' => Active::YES,
    ];
    protected $hidden = ['created_at', 'updated_at'];
}
