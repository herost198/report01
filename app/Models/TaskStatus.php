<?php

namespace App\Models;

use App\Enums\StatusTaskStatus;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    protected $table = 'task_status';

    protected $fillable = [
        'status', 'start_time', 'end_time', 'time_working', 'task_id', 'user_id', 'is_active'
    ];
    protected $attributes = [
        'status' => StatusTaskStatus::OPEN,
        'is_active' => false,
    ];

    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
