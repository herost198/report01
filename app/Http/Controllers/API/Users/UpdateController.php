<?php

namespace App\Http\Controllers\API\Users;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function update(Request $request, $id = '')
    {
        $user = Auth::user();
        $inputValidate = $this->validateInput($request, $user);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_user.update_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $password = trim($request['new_password']);
        $params = [
            'full_name' => $request['full_name'],
            'phone' => $request['phone'],
        ];
        if ($user->role === UserRole::ADMIN) {
            $params['role'] = $request['role'];
            if (!empty($password)) {
                $params['password'] = Hash::make($password);
            }
        }
        if ($id !== '') {
            $updated = $this->userService->updateUser($id, $params);
        } else {
            $updated = $this->userService->updateUser($user->id, $params);
        }
        if (!empty($updated)) {
            return response()->json([
                'message' => config('constants.message_user.update_success'),
                'user' => $updated
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => config('constants.message_user.update_error'),
        ], Response::HTTP_BAD_REQUEST);
    }

    public function validateInput(Request $request, $user)
    {
        $rules = [
            'full_name' => 'required|string|max:127',
            'phone' => 'nullable|regex:/(0)+([0-9]{9,10})\b/',
        ];
        if ($user->role === UserRole::ADMIN && trim($request['new_password'])) {
            $rules['new_password'] = 'required|string';
            $rules['new_password_confirm'] = 'required|same:new_password';
        }
        return Validator::make($request->all(), $rules);
    }
}
