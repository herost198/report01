<?php

namespace App\Http\Controllers\API\Users;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IndexController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $getAll = $request->query('all');
        if ($getAll) {
            $users = $this->userService->all(null, !$getAll);
            return response()->json([
                'success' => 'success',
                'users' => $users,
            ], Response::HTTP_OK);
        }
        $search = $request->query('search');
        $users = $this->userService->all($search);
        $pagination = [
            'total' => $users->total(),
            'per_page' => $users->perPage(),
            'current_page' => $users->currentPage(),
            'last_page' => $users->lastPage(),
            'from' => $users->firstItem(),
            'to' => $users->lastItem()
        ];
        return response()->json([
            'success' => 'success',
            'users' => $users,
            'pagination' => $pagination,
        ], Response::HTTP_OK);
    }
}
