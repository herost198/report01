<?php

namespace App\Http\Controllers\API\Users;

use App\Enums\UserStatus;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Response;

class DeleteController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function destroy($id)
    {
        $params = [
            'status' => UserStatus::INACTIVE
        ];
        try {
            if (!empty($this->userService->updateUser($id, $params))) {
                return response()->json([
                    'message' => config('constants.message_user.delete_success')
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_user.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_user.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
