<?php

namespace App\Http\Controllers\API\Users;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function store(Request $request)
    {
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_user.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = [
            'full_name' => $request['full_name'],
            'email' => $request['email'],
            'role' => $request['role'],
            'phone' => $request['phone'],
            'password' => Hash::make($request['password']),
        ];
        $create = $this->userService->store(($params));
        if (!empty($create)) {
            return response()->json([
                'message' => config('constants.message_user.create_success'),
                'user' => $create
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => config('constants.message_user.create_error'),
        ], Response::HTTP_BAD_REQUEST);
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'full_name' => 'required|string|max:127',
            'email' => 'required|string|max:127|unique:user,email',
            'role' => ['required', new EnumValue(UserRole::class, false)],
            'phone' => 'nullable|regex:/(0)+([0-9]{9,10})\b/',
            'password' => 'required|string',
            'password_confirm' => 'required|same:password'
        ]);
    }
}
