<?php

namespace App\Http\Controllers\API\ProjectAttributes;

use App\Http\Controllers\Controller;
use App\Services\ProjectAttributeService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    protected $projectAttributeService;
    protected $projectMemberService;

    public function __construct(
        ProjectAttributeService $projectAttributeService,
        ProjectMemberService $proMemberService
    ) {
        $this->projectAttributeService = $projectAttributeService;
        $this->projectMemberService = $proMemberService;
    }

    public function store(Request $request)
    {
        $errResponse = response()->json([
            'message' => config('constants.message_project_attribute.create_error'),
        ], Response::HTTP_BAD_REQUEST);

        if (!$this->projectMemberService->hasPermissionByProjectId($request['project_id'])) {
            return $errResponse;
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_attribute.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = $request->only(['project_id', 'attribute_key', 'attribute_name']);
        try {
            $create = $this->projectAttributeService->store($params);
            if (!empty($create)) {
                return response()->json([
                    'message' => config('constants.message_project_attribute.create_success'),
                ], Response::HTTP_OK);
            }
            return $errResponse;
        } catch (\Exception $e) {
            return $errResponse;
        }
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'project_id' => 'required|numeric|exists:project,id',
            'attribute_key' => 'required|max:127|regex:/^[A-Z0-9_.-]*$/',
            'attribute_name' => 'required|string|max:127',
        ]);
    }
}
