<?php

namespace App\Http\Controllers\API\ProjectAttributes;

use App\Http\Controllers\Controller;
use App\Services\ProjectAttributeService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class DestroyController extends Controller
{
    protected $projectAttributeService;
    protected $projectMemberService;

    public function __construct(
        ProjectAttributeService $projectAttributeService,
        ProjectMemberService $projMemberService
    ) {
        $this->projectAttributeService = $projectAttributeService;
        $this->projectMemberService = $projMemberService;
    }

    public function destroy($attributeKey, $projectId)
    {
        try {
            if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
                return response()->json([
                    'message' => config('constants.message_user.permission_error')
                ], Response::HTTP_FORBIDDEN);
            }
            $deleted = $this->projectAttributeService->destroyByAttrKeyAndProId($attributeKey, $projectId);
            if ($deleted) {
                return response()->json([
                    'message' => config('constants.message_project_attribute.delete_success'),
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_attribute.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_attribute.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
