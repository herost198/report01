<?php

namespace App\Http\Controllers\API\ProjectAttributes;

use App\Http\Controllers\Controller;
use App\Services\ProjectAttributeService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    protected $projectAttributeService;
    protected $projectMemberService;

    public function __construct(
        ProjectAttributeService $projectAttributeService,
        ProjectMemberService $projectMemberService
    ) {
        $this->projectAttributeService = $projectAttributeService;
        $this->projectMemberService = $projectMemberService;
    }

    public function update($attributeKey, $projectId, Request $request)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
            return response()->json([
                'message' => config('constants.message_user.permission_error')
            ], Response::HTTP_FORBIDDEN);
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_attribute.update_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $attributeKeyInput = trim($request['attribute_key']);
        $params = [
            'attribute_name' => $request['attribute_name'],
        ];
        if (!empty($attributeKeyInput)) {
            $params['attribute_key'] = $attributeKeyInput;
        }
        try {
            $updated = $this->projectAttributeService->updateProjectAttribute($attributeKey, $projectId, $params);
            if (!empty($updated)) {
                return response()->json([
                    'message' => config('constants.message_project_attribute.update_success'),
                    'projectAttribute' => $updated
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_attribute.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_attribute.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function validateInput(Request $request)
    {
        $rules = [
            'attribute_key' => 'nullable|max:127|regex:/^[A-Z0-9_.-]*$/',
            'attribute_name' => 'required|string|max:127',
        ];
        return Validator::make($request->all(), $rules);
    }
}
