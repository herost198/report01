<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    protected $useService;

    public function __construct(UserService $userService)
    {
        $this->useService = $userService;
    }

    public function getUser()
    {
        $user = Auth::user();
        if ($user != null) {
            return response()->json([
                'success' => 'success',
                'data' => $user
            ], Response::HTTP_OK);
        }
        return response()->json([
            'error' => 'error',
        ], Response::HTTP_BAD_REQUEST);
    }
}
