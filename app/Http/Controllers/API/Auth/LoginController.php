<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthService;

class LoginController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function login()
    {
        return $this->authService->login();
    }

    public function logout()
    {
        return $this->authService->logout();
    }
    public function refresh()
    {
        return $this->authService->refresh();
    }
}
