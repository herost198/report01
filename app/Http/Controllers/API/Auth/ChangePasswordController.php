<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class ChangePasswordController extends Controller
{
    protected $useService;

    public function __construct(UserService $userService)
    {
        $this->useService = $userService;
    }

    public function changePassword(Request $request)
    {
        $validator = $this->validateChangePassword($request->all());
        if ($validator->fails()) {
            return response()->json([
                "error" => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $userId = Auth::user()->id;
        $newPassword = $request['new_password'];
        $oldPassword = $request['old_password'];
        $checkPassword = $this->useService->checkPassword($oldPassword, $userId);
        if ($checkPassword) {
            $user = [
                'password' => bcrypt($newPassword)
            ];
            $this->useService->updateUser($userId, $user);
            return response()->json([], Response::HTTP_OK);
        }
        return response()->json([], Response::HTTP_BAD_REQUEST);
    }

    public function validateChangePassword($inputs)
    {
        return Validator::make($inputs, [
            'old_password' => 'required|max:255|min:8',
            'new_password' => 'required|max:255|min:8',
            'new_password_confirm' => 'required|same:new_password',
        ]);
    }
}
