<?php

namespace App\Http\Controllers\API\Annotations;

use App\Http\Controllers\Controller;
use App\Services\TaskService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShowTaskController extends Controller
{

    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param int $taskID
     * @param Request $request
     * @return JsonResponse
     */
    public function main(int $taskID, Request $request)
    {
        $response = $this->taskService->getByID($taskID);
        return response()->json($response, 200);
    }
}
