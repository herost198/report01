<?php

namespace App\Http\Controllers\API\Annotations;

use App\Http\Controllers\Controller;
use App\Services\TaskService;
use App\Services\TaskStatusService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UpdateTaskController extends Controller
{

    private $taskService;
    private $taskStatusService;

    public function __construct(TaskService $taskService, TaskStatusService $taskStatusService)
    {
        $this->taskService = $taskService;
        $this->taskStatusService = $taskStatusService;
    }

    /**
     * @param int $taskID
     * @param Request $request
     * @return JsonResponse
     */
    public function main(int $taskID, Request $request)
    {
        $jsonOutput = $request->only('output_json');
        $timeWorking = $request->only('time_working');
        DB::beginTransaction();
        try {
            $this->taskService->updateByID($taskID, $jsonOutput);
            $this->taskStatusService->update($request['task_status_id'], $timeWorking);
            DB::commit();
            return response()->json([
                'message' => config('constants.message_task_status.update_success'),
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => config('constants.message_task_status.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
