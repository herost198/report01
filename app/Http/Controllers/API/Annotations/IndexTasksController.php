<?php

namespace App\Http\Controllers\API\Annotations;

use App\Http\Controllers\Controller;
use App\Services\TaskService;
use Illuminate\Http\Request;

class IndexTasksController extends Controller
{
    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @param int $projectID
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function main(int $projectID)
    {
        $tasks = $this->taskService->findByProjectID($projectID);
        $pagination = [
            'total' => $tasks['total'],
            'per_page' => $tasks['per_page'],
            'current_page' => $tasks['current_page'],
            'last_page' => $tasks['last_page'],
            'from' => $tasks['from'],
            'to' => $tasks['to']
        ];
        $tasks = $tasks['data'];
        for ($i = 0; $i < count($tasks); $i++) {
            if (!empty($tasks[$i]['output_json'])) {
                $tasks[$i]['labels'] = json_decode($tasks[$i]['output_json'], true);
            } else {
                $tasks[$i]['labels'] = [];
            }
            unset($tasks[$i]['output_json']);
        }
        $dataReturn = [
            'tasks' => $tasks,
            'pagination' => $pagination
        ];
        return response()->json($dataReturn, 200);
    }
}
