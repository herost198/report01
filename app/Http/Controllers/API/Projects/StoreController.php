<?php

namespace App\Http\Controllers\API\Projects;

use App\Enums\ProjectStatus;
use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Services\FlowService;
use App\Services\ProjectMemberService;
use App\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    protected $projectService;
    protected $flowService;
    protected $projectMemberService;

    public function __construct(
        ProjectService $projectService,
        FlowService $flowService,
        ProjectMemberService $projectMemberService
    ) {
        $this->projectService = $projectService;
        $this->flowService = $flowService;
        $this->projectMemberService = $projectMemberService;
    }

    public function store(Request $request)
    {
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = $request->only(['name', 'key', 'start_at', 'end_at', 'note']);
        $params['status'] = ProjectStatus::OPEN;
        $params['created_at'] = date('Y-m-d H:i:s');
        $params['updated_at'] = date('Y-m-d H:i:s');
        DB::beginTransaction();
        try {
            $flows = $request['flows'];
            $create = $this->projectService->store($params);
            $projectId = $create->id;
            $user = Auth::user();
            if ($user->role == UserRole::PM) {
                $paramsMember['user_id'] = $user->id;
                $paramsMember['project_id'] = $projectId;
                $create = $this->projectMemberService->store($paramsMember);
            }
            foreach ($flows as $key => $flow) {
                $dataFlow = array();
                $dataFlow['project_id'] = $projectId;
                $dataFlow['order'] = ++$key;
                $dataFlow['step_name'] = $flow['name'];
                $this->flowService->store($dataFlow);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => config('constants.message_project.create_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
        return response()->json([
            'message' => config('constants.message_project.create_success'),
            'project' => $create
        ], Response::HTTP_OK);
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:127',
            'key' => 'required|max:127|unique:project,key|regex:/^[A-Z0-9_.-]*$/',
            'start_at' => 'date_format:Y-m-d|before_or_equal:end_at',
            'end_at' => 'date_format:Y-m-d|after_or_equal:start_at',
            'flows.*.name' => 'required',
        ]);
    }
}
