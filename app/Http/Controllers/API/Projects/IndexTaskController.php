<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\TaskService;
use Illuminate\Http\Response;

class IndexTaskController extends Controller
{
    protected $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function main($projectID)
    {
        $tasks = $this->taskService->findByProjectID($projectID);
        $pagination = [
            'total' => $tasks['total'],
            'per_page' => $tasks['per_page'],
            'current_page' => $tasks['current_page'],
            'last_page' => $tasks['last_page'],
            'from' => $tasks['from'],
            'to' => $tasks['to']
        ];
        $tasks = $tasks['data'];
        for ($i = 0; $i < count($tasks); $i++) {
            if (!empty($tasks[$i]['output_json'])) {
                $tasks[$i]['labels'] = json_decode($tasks[$i]['output_json'], true);
            } else {
                $tasks[$i]['labels'] = [];
            }
            unset($tasks[$i]['output_json']);
        }
        return response()->json([
            'tasks' => $tasks,
            'pagination' => $pagination,
        ], Response::HTTP_OK);
    }
}
