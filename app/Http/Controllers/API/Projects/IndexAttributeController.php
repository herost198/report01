<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\ProjectAttributeService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class IndexAttributeController extends Controller
{
    protected $projectMemberService;
    protected $projectAttributeService;

    public function __construct(
        ProjectMemberService $projMemberService,
        ProjectAttributeService $projectAttributeService
    ) {
        $this->projectMemberService = $projMemberService;
        $this->projectAttributeService = $projectAttributeService;
    }

    public function main($projectID)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectID)) {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
        $projectAttributes = $this->projectAttributeService->getByProjectId($projectID);
        return response()->json([
            'projectAttributes' => $projectAttributes
        ], Response::HTTP_OK);
    }
}
