<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class IndexCategoryController extends Controller
{
    protected $projectMemberService;
    protected $projectCategoryService;

    public function __construct(ProjectMemberService $projMemberService, ProjectCategoryService $projCategoryService)
    {
        $this->projectMemberService = $projMemberService;
        $this->projectCategoryService = $projCategoryService;
    }

    public function main($projectID)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectID)) {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
        $projectCategories = $this->projectCategoryService->getByProjectId($projectID);
        return response()->json([
            'projectCategories' => $projectCategories
        ], Response::HTTP_OK);
    }
}
