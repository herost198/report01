<?php

namespace App\Http\Controllers\API\Projects;

use App\Enums\ProjectStatus;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    protected $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function update($id, Request $request)
    {
        $inputValidate = $this->validateInput($request);
        $checkStatusProject = $this->projectService->checkStatusProject($id);
        if ($inputValidate->fails() || $request['status'] == ProjectStatus::OPEN && $checkStatusProject == false) {
            return response()->json([
                'message' => config('constants.message_project.update_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = $request->only(['name', 'start_at', 'end_at', 'note', 'status', 'key']);
        $params['updated_at'] = date('Y-m-d H:i:s');
        $updated = $this->projectService->update($id, $params);
        if (!empty($updated)) {
            return response()->json([
                'message' => config('constants.message_project.update_success'),
                'project' => $updated,
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => config('constants.message_project.update_error'),
        ], Response::HTTP_BAD_REQUEST);
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => 'required|string|max:127',
            'status' => ['required', new EnumValue(ProjectStatus::class, false)],
            'start_at' => 'date_format:Y-m-d|before_or_equal:end_at',
            'end_at' => 'date_format:Y-m-d|after_or_equal:start_at',
            'key' => 'required|max:127|unique:project,key|regex:/^[A-Z0-9_.-]*$/',
        ]);
    }
}
