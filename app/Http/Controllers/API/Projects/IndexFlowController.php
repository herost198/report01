<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\FlowService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class IndexFlowController extends Controller
{
    protected $flowService;
    protected $projectMemberService;

    public function __construct(FlowService $flowService, ProjectMemberService $projectMemberService)
    {
        $this->flowService = $flowService;
        $this->projectMemberService = $projectMemberService;
    }

    public function main($projectID)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectID);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $flows = $this->flowService->getByProjectID($projectID);
        return response()->json([
            'flows' => $flows
        ], Response::HTTP_OK);
    }
}
