<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\FlowService;
use App\Services\ProjectAttributeService;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use App\Services\ProjectService;
use Illuminate\Http\Response;
use DB;

class DestroyController extends Controller
{
    protected $projectService;
    protected $projectMemberService;
    protected $projectCategoryService;
    protected $flowService;
    protected $projectAttributeService;

    public function __construct(
        ProjectService $projectService,
        ProjectMemberService $projectMemberService,
        ProjectCategoryService $projectCategoryService,
        FlowService $flowService,
        ProjectAttributeService $projectAttributeService
    ) {
        $this->projectService = $projectService;
        $this->projectMemberService = $projectMemberService;
        $this->projectCategoryService = $projectCategoryService;
        $this->flowService = $flowService;
        $this->projectAttributeService = $projectAttributeService;
    }

    public function destroy($id)
    {
        $checkProject = $this->projectService->checkStatusProject($id);
        if ($checkProject) {
            DB::beginTransaction();
            try {
                $this->projectMemberService->deleteProjectMember($id);
                $this->projectCategoryService->destroyByProjectId($id);
                $this->projectAttributeService->destroyByProjectId($id);
                $this->flowService->destroyByProjectId($id);
                $this->projectService->delete($id);
                DB::commit();
                return response()->json([
                    'message' => config('constants.message_project.delete_success')
                ], Response::HTTP_OK);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'message' => config('constants.message_project.delete_error'),
                ], Response::HTTP_BAD_REQUEST);
            }
        }
        return response()->json([
            'message' => config('constants.message_project.delete_error_status')
        ], Response::HTTP_BAD_REQUEST);
    }
}
