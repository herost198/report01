<?php

namespace App\Http\Controllers\API\Projects;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use App\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    protected $projectService;
    protected $projectMemberService;

    public function __construct(ProjectService $projectService, ProjectMemberService $projectMemberService)
    {
        $this->projectService = $projectService;
        $this->projectMemberService = $projectMemberService;
    }

    public function index(Request $request)
    {
        $paramsSearch = array();
        $paramsSearch['search'] = $request->query('search');
        $paramsSearch['statuses'] = $request->query('statuses');
        if (Auth::user()->role == UserRole::ADMIN) {
            $projects = $this->projectService->all(true, $paramsSearch);
        } else {
            $userId = Auth::user()->id;
            $projectMembers = $this->projectMemberService->getProjectMemberByUserIds($userId);
            $arrProjectId = $projectMembers->pluck('project_id');
            $projects = $this->projectService->getProjectByProjectIds($arrProjectId, $paramsSearch);
        }
        $pagination = [
            'total' => $projects->total(),
            'per_page' => $projects->perPage(),
            'current_page' => $projects->currentPage(),
            'last_page' => $projects->lastPage(),
            'from' => $projects->firstItem(),
            'to' => $projects->lastItem()
        ];
        return response()->json([
            'message' => 'success',
            'projects' => $projects,
            'pagination' => $pagination,
        ], Response::HTTP_OK);
    }

    public function getAll()
    {
        $projects = $this->projectService->all(false);
        return response()->json($projects, Response::HTTP_OK);
    }
}
