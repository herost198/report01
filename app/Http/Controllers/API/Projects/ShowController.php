<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use App\Services\ProjectService;
use Illuminate\Http\Response;

class ShowController extends Controller
{
    protected $projectService;
    protected $projectMemberService;

    public function __construct(ProjectService $projectService, ProjectMemberService $projectMemberService)
    {
        $this->projectService = $projectService;
        $this->projectMemberService = $projectMemberService;
    }

    public function show($id)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($id)) {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
        $project = $this->projectService->getProjectByProjectId($id);
        return response()->json([
            'project' => $project,
        ], Response::HTTP_OK);
    }
}
