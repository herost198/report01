<?php

namespace App\Http\Controllers\API\Projects;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class IndexMemberController extends Controller
{
    protected $projectMemberService;

    public function __construct(ProjectMemberService $projectMemberService)
    {
        $this->projectMemberService = $projectMemberService;
    }

    public function main($projectID)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectID);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $projectMembers = $this->projectMemberService->getByProjectId($projectID);
        return response()->json([
            'projectMembers' => $projectMembers
        ], Response::HTTP_OK);
    }
}
