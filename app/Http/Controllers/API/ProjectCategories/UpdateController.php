<?php

namespace App\Http\Controllers\API\ProjectCategories;

use App\Http\Controllers\Controller;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    protected $projectCategoryService;
    protected $projectMemberService;

    public function __construct(ProjectCategoryService $projectCategoryService, ProjectMemberService $projMemberService)
    {
        $this->projectCategoryService = $projectCategoryService;
        $this->projectMemberService = $projMemberService;
    }

    public function edit($categoryKey, $projectId)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
            return response()->json([
                'message' => config('constants.message_user.permission_error')
            ], Response::HTTP_FORBIDDEN);
        }
        $projectCategory = $this->projectCategoryService->getProjectCategory($categoryKey, $projectId);
        if (!empty($projectCategory)) {
            return response()->json([
                'data' => $projectCategory
            ], Response::HTTP_OK);
        }
        return response()->json([
            'message' => config('constants.message_project_category.show_error'),
        ], Response::HTTP_BAD_REQUEST);
    }

    public function update($categoryKey, $projectId, Request $request)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
            return response()->json([
                'message' => config('constants.message_user.permission_error')
            ], Response::HTTP_FORBIDDEN);
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_category.update_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $categoryKeyInput = trim($request['category_key']);
        $params = [
            'category_name' => $request['category_name'],
        ];
        if (!empty($categoryKeyInput)) {
            $params['category_key'] = $categoryKeyInput;
        }
        try {
            $updated = $this->projectCategoryService->updateProjectCategory($categoryKey, $projectId, $params);
            if (!empty($updated)) {
                return response()->json([
                    'message' => config('constants.message_project_category.update_success'),
                    'projectCategory' => $updated
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_category.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_category.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function validateInput(Request $request)
    {
        $rules = [
            'category_key' => 'nullable|max:127|regex:/^[A-Z0-9_.-]*$/',
            'category_name' => 'required|string|max:127',
        ];
        return Validator::make($request->all(), $rules);
    }
}
