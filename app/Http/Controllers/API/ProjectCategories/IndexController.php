<?php

namespace App\Http\Controllers\API\ProjectCategories;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    protected $projectCategoryService;
    protected $projectMemberService;

    public function __construct(ProjectCategoryService $proCategoryService, ProjectMemberService $proMemberService)
    {
        $this->projectCategoryService = $proCategoryService;
        $this->projectMemberService = $proMemberService;
    }

    public function index()
    {
        $user = Auth::user();
        if ($user->role == UserRole::ADMIN) {
            $projectCategories = $this->projectCategoryService->all();
        } else {
            $projectMembers = $this->projectMemberService->getProjectMemberByUserIds($user->id);
            $arrProjectIds = $projectMembers->pluck('project_id');
            $projectCategories = $this->projectCategoryService->getProjectCategoryByProjectIds($arrProjectIds);
        }
        $pagination = [
            'total' => $projectCategories->total(),
            'per_page' => $projectCategories->perPage(),
            'current_page' => $projectCategories->currentPage(),
            'last_page' => $projectCategories->lastPage(),
            'from' => $projectCategories->firstItem(),
            'to' => $projectCategories->lastItem()
        ];
        return response()->json([
            'success' => 'success',
            'projectCategories' => $projectCategories,
            'pagination' => $pagination
        ], Response::HTTP_OK);
    }
}
