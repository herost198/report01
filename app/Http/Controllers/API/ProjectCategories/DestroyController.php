<?php

namespace App\Http\Controllers\API\ProjectCategories;

use App\Http\Controllers\Controller;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class DestroyController extends Controller
{
    protected $projectCategoryService;
    protected $projectMemberService;

    public function __construct(ProjectCategoryService $projectCategoryService, ProjectMemberService $projMemberService)
    {
        $this->projectCategoryService = $projectCategoryService;
        $this->projectMemberService = $projMemberService;
    }

    public function destroy($categoryKey, $projectId)
    {
        try {
            if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
                return response()->json([
                    'message' => config('constants.message_user.permission_error')
                ], Response::HTTP_FORBIDDEN);
            }
            $deleted = $this->projectCategoryService->destroyByCatKeyAndProId($categoryKey, $projectId);
            if ($deleted) {
                return response()->json([
                    'message' => config('constants.message_project_category.delete_success'),
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_category.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_category.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
