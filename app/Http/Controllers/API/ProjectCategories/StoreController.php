<?php

namespace App\Http\Controllers\API\ProjectCategories;

use App\Http\Controllers\Controller;
use App\Services\ProjectCategoryService;
use App\Services\ProjectMemberService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    protected $projectCategoryService;
    protected $projectMemberService;

    public function __construct(ProjectCategoryService $proCategoryService, ProjectMemberService $proMemberService)
    {
        $this->projectCategoryService = $proCategoryService;
        $this->projectMemberService = $proMemberService;
    }

    public function store(Request $request)
    {
        $errResponse = response()->json([
            'message' => config('constants.message_project_category.create_error'),
        ], Response::HTTP_BAD_REQUEST);

        if (!$this->projectMemberService->hasPermissionByProjectId($request['project_id'])) {
            return $errResponse;
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_category.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = $request->only(['project_id', 'category_key', 'category_name']);
        try {
            $create = $this->projectCategoryService->store(($params));
            if (!empty($create)) {
                return response()->json([
                    'message' => config('constants.message_project_category.create_success'),
                ], Response::HTTP_OK);
            }
            return $errResponse;
        } catch (\Exception $e) {
            return $errResponse;
        }
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'project_id' => 'required|numeric|exists:project,id',
            'category_key' => 'required|max:127|regex:/^[A-Z0-9_.-]*$/',
            'category_name' => 'required|string|max:127',
        ]);
    }
}
