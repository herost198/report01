<?php

namespace App\Http\Controllers\API\Tasks;

use App\Enums\StatusTask;
use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use App\Services\TaskService;
use App\Services\TaskStatusService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UpdateController extends Controller
{
    protected $taskService;
    protected $projectMemberService;
    protected $taskStatusService;

    public function __construct(
        TaskService $taskService,
        ProjectMemberService $projectMemberService,
        TaskStatusService $taskStatusService
    ) {
        $this->taskService = $taskService;
        $this->projectMemberService = $projectMemberService;
        $this->taskStatusService = $taskStatusService;
    }

    public function update($projectId, $taskId, Request $request)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectId)) {
            return response()->json([
                'message' => config('constants.message_user.permission_error')
            ], Response::HTTP_FORBIDDEN);
        }
        $task = $this->taskService->getByID($taskId);
        $inputValidate = $this->validateInput($request, $projectId, $task->status);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_task.update_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $paramsTask['title'] = $request['title'];
        DB::beginTransaction();
        try {
            if ($task->status == StatusTask::UNASSIGN) {
                $paramsTaskStatus = $request->only(['user_id', 'start_time', 'end_time']);
                $paramsTaskStatus['task_id'] = $taskId;
                $paramsTaskStatus['is_active'] = true;
                $this->taskStatusService->store($paramsTaskStatus);

                $paramsTaskStatus['is_active'] = false;
                $paramsTask['status'] = StatusTask::OPEN;
                $reviewersSelected = $request['reviewersSelected'];
                foreach ($reviewersSelected as $reviewer) {
                    $paramsTaskStatus['user_id'] = $reviewer;
                    $this->taskStatusService->store($paramsTaskStatus);
                }
            } else {
                $paramsTask['status'] = $request['status'];
            }
            $this->taskService->updateByID($taskId, $paramsTask);
            DB::commit();
            return response()->json([
                'message' => config('constants.message_project_task.update_success'),
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'message' => config('constants.message_project_task.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function validateInput($request, $projectId, $status = null)
    {
        $arrayUserOfProject = $this->projectMemberService->getArrayUserIdByProjectId($projectId);
        $rules = [
            'title' => 'required|string|max:127',
        ];
        if ($status == StatusTask::UNASSIGN) {
            $rules['user_id'] = 'required|exists:project_member,user_id,project_id,' . $projectId;
            $rules['start_time'] = 'required|date_format:Y-m-d|before_or_equal:end_time';
            $rules['end_time'] = 'required|date_format:Y-m-d|after_or_equal:start_time';
            $rules['reviewersSelected.*'] = ['required', Rule::in($arrayUserOfProject)];
        }
        return Validator::make($request->all(), $rules);
    }
}
