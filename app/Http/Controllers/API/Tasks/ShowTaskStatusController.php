<?php

namespace App\Http\Controllers\API\Tasks;

use App\Http\Controllers\Controller;
use App\Services\TaskStatusService;
use App\Services\TaskService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ShowTaskStatusController extends Controller
{
    protected $taskService;
    protected $taskStatusService;

    public function __construct(TaskService $taskService, TaskStatusService $taskStatusService)
    {
        $this->taskService = $taskService;
        $this->taskStatusService = $taskStatusService;
    }

    public function main($taskId, $taskStatusId)
    {
        $task = $this->taskService->getByID($taskId);
        $taskStatus = $this->taskStatusService->getByID($taskStatusId);
        if (!empty($task['output_json'])) {
            $task['labels'] = json_decode($task['output_json'], true);
        } else {
            $task['labels'] = [];
        }
        unset($task['output_json']);
        return response()->json([
            'task' => $task,
            'taskStatus' => $taskStatus,
        ], Response::HTTP_OK);
    }
}
