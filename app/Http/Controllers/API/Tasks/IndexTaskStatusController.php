<?php

namespace App\Http\Controllers\API\Tasks;

use App\Http\Controllers\Controller;
use App\Services\TaskStatusService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IndexTaskStatusController extends Controller
{
    protected $taskStatusService;

    public function __construct(TaskStatusService $taskStatusService)
    {
        $this->taskStatusService = $taskStatusService;
    }

    public function main($taskId)
    {
        $taskStatus = $this->taskStatusService->getByTaskID($taskId);
        return response()->json([
            'taskStatus' => $taskStatus,
        ], Response::HTTP_OK);
    }
}
