<?php

namespace App\Http\Controllers\API\Tasks;

use App\Http\Controllers\Controller;
use App\Services\TaskService;
use App\Services\TaskStatusService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DestroyController extends Controller
{
    protected $taskStatusService;
    protected $taskService;

    public function __construct(TaskStatusService $taskStatusService, TaskService $taskService)
    {
        $this->taskStatusService = $taskStatusService;
        $this->taskService = $taskService;
    }

    public function destroy($taskId)
    {
        DB::beginTransaction();
        try {
            $taskImage = $this->taskService->getByID($taskId)->src_url;
            $this->taskStatusService->deleteByTaskId($taskId);
            $this->taskService->deleteById($taskId);
            DB::commit();
            $taskImage = str_replace('/storage', '', $taskImage);
            Storage::delete('/public' . $taskImage);
            return response()->json([
                'message' => config('constants.message_project_task.delete_success')
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => config('constants.message_project_task.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
