<?php

namespace App\Http\Controllers\API\Tasks;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use App\Services\ProjectService;
use App\Services\TaskService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class StoreController extends Controller
{
    protected $taskService;
    protected $projectMemberService;
    protected $projectService;

    public function __construct(
        TaskService $taskService,
        ProjectMemberService $projectMemberService,
        ProjectService $projectService
    ) {
        $this->taskService = $taskService;
        $this->projectMemberService = $projectMemberService;
        $this->projectService = $projectService;
    }

    public function store($projectID, Request $request)
    {
        if (!$this->projectMemberService->hasPermissionByProjectId($projectID)) {
            return response()->json([
                'message' => config('constants.message_project_task.create_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'message' => config('constants.message_project_task.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params['title'] = $request['title'];
        $image = $request['file'];
        $params['project_id'] = $projectID;
        try {
            $project = $this->projectService->findByID($projectID);
            $imageName = $project->key . "_" . time() . "_" . $image->getClientOriginalName();
            Storage::disk('public')->put('images/' . $imageName, File::get($image->getRealPath()));
            $params['src_url'] = '/storage/images/' . $imageName;
            $create = $this->taskService->store($params);
            if (!empty($create)) {
                return response()->json([
                    'message' => config('constants.message_project_task.create_success'),
                    'task' => $create
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_task.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_task.create_error'),
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'title' => 'required|string|max:127',
            'file' => 'required|file|mimes:jpeg,jpg,bmp,png',
        ]);
    }
}
