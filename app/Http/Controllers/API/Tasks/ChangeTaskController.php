<?php

namespace App\Http\Controllers\API\Tasks;

use App\Enums\StatusTask;
use App\Http\Controllers\Controller;
use App\Services\TaskService;
use App\Services\TaskStatusService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChangeTaskController extends Controller
{
    protected $taskService;
    protected $taskStatusService;

    public function __construct(TaskService $taskService, TaskStatusService $taskStatusService)
    {
        $this->taskService = $taskService;
        $this->taskStatusService = $taskStatusService;
    }

    public function main($taskId)
    {
        $user = Auth::user();
        $getHasActiveByTaskID = $this->taskStatusService->getHasActiveByTaskID($taskId);
        // get records has unique
        $getlistUserID = array_unique($this->taskStatusService->getListUserIDByTaskID($taskId));
        // remove element first in array
        array_shift($getlistUserID);
        $reviewers = $getlistUserID;
        if ($getHasActiveByTaskID->count() >= 2 ||
            $getHasActiveByTaskID->first()->user_id != $user->id &&
            in_array($getHasActiveByTaskID->first()->user_id, $reviewers)
        ) {
            return response()->json([], 400);
        }
        DB::beginTransaction();
        try {
            $this->taskService->updateByID($taskId, ['status' => StatusTask::FIXING]);
            $firstTaskStatusByTaskId = $this->taskStatusService->firstByTaskId($taskId);
            $params = [
                'user_id' => $firstTaskStatusByTaskId->user_id,
                'start_time' => date('Y-m-d H:i:s'),
                'task_id' => $taskId,
                'end_time' => $firstTaskStatusByTaskId->end_time,
                'is_active' => true,
            ];
            $this->taskStatusService->store($params);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 400);
        }
        return response()->json([
            "message" => "Change task successfully!"
        ], 200);
    }
}
