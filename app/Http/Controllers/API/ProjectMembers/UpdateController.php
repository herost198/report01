<?php

namespace App\Http\Controllers\API\ProjectMembers;

use App\Enums\Active;
use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
    protected $projectMemberService;

    public function __construct(ProjectMemberService $projectMemberService)
    {
        $this->projectMemberService = $projectMemberService;
    }

    public function update($projectId, $userId, Request $request)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectId);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $param['is_active'] = $request['is_active'];
        try {
            $updated = $this->projectMemberService->updateProMemberByProIdAndUserId($projectId, $userId, $param);
            if (!empty($updated)) {
                return response()->json([
                    'message' => config('constants.message_project_member.update_success')
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_member.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_member.update_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'is_active' => ['required', new EnumValue(Active::class, false)],
        ]);
    }
}
