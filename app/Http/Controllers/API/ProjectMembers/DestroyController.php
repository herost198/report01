<?php

namespace App\Http\Controllers\API\ProjectMembers;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DestroyController extends Controller
{
    protected $projectMemberService;

    public function __construct(ProjectMemberService $projectMemberService)
    {
        $this->projectMemberService = $projectMemberService;
    }

    public function destroy($projectId, $userId)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectId);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        try {
            $deleted = $this->projectMemberService->destroyByProIdAndUserId($projectId, $userId);
            if ($deleted) {
                return response()->json([
                    'message' => config('constants.message_project_member.delete_success'),
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_member.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_member.delete_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }
}
