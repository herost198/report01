<?php

namespace App\Http\Controllers\API\ProjectMembers;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    protected $projectMemberService;

    public function __construct(ProjectMemberService $projectMemberService)
    {
        $this->projectMemberService = $projectMemberService;
    }

    public function store($projectId, Request $request)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectId);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $inputValidate = $this->validateInput($request);
        if ($inputValidate->fails()) {
            return response()->json([
                'error' => $inputValidate->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params['user_id'] = $request['user_id'];
        $params['project_id'] = $projectId;
        try {
            $create = $this->projectMemberService->store($params);
            if (!empty($create)) {
                return response()->json([
                    'message' => config('constants.message_project_member.create_success'),
                ], Response::HTTP_OK);
            }
            return response()->json([
                'message' => config('constants.message_project_member.create_error'),
            ], Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return response()->json([
                'message' => config('constants.message_project_member.create_error'),
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    public function validateInput(Request $request)
    {
        return Validator::make($request->all(), [
            'user_id' => 'required|exists:user,id',
        ]);
    }
}
