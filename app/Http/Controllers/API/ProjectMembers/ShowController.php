<?php

namespace App\Http\Controllers\API\ProjectMembers;

use App\Http\Controllers\Controller;
use App\Services\ProjectMemberService;
use Illuminate\Http\Response;

class ShowController extends Controller
{
    protected $projectMemberService;

    public function __construct(ProjectMemberService $projectMemberService)
    {
        $this->projectMemberService = $projectMemberService;
    }

    public function show($projectId)
    {
        $checked = $this->projectMemberService->hasPermissionByProjectId($projectId);
        if (!$checked) {
            return response()->json([
                'message' => config('constants.message_project_member.permission_error'),
            ], Response::HTTP_FORBIDDEN);
        }
        $projectMembers = $this->projectMemberService->getByProjectId($projectId);
        return response()->json([
            'projectMembers' => $projectMembers,
        ], Response::HTTP_OK);
    }
}
