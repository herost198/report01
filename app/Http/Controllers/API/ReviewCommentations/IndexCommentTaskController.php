<?php

namespace App\Http\Controllers\API\ReviewCommentations;

use App\Http\Controllers\Controller;
use App\Services\ReviewCommentationService;
use Illuminate\Http\Request;

class IndexCommentTaskController extends Controller
{
    private $reviewCmtService;

    public function __construct(ReviewCommentationService $reviewCmtService)
    {
        $this->reviewCmtService = $reviewCmtService;
    }

    /**
     * @param int $taskID
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function main(int $taskID)
    {
        $reviewCmts = $this->reviewCmtService->findByTaskID($taskID);
        return response()->json($reviewCmts, 200);
    }
}
