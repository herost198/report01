<?php

namespace App\Http\Controllers\API\ReviewCommentations;

use App\Http\Controllers\Controller;
use App\Services\ReviewCommentationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StoreController extends Controller
{
    private $reviewCmtService;

    public function __construct(ReviewCommentationService $reviewCmtService)
    {
        $this->reviewCmtService = $reviewCmtService;
    }

    public function main(Request $request)
    {
        $validateRequest = $this->validateRequest($request);
        if ($validateRequest->fails()) {
            return response()->json([
                'error' => $validateRequest->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $params = $request->only('user_id', 'task_id', 'comment');
        $response = $this->reviewCmtService->store($params);
        return response()->json($response, 200);
    }
    public function validateRequest(Request $request)
    {
        return Validator::make($request->all(), [
          'user_id' => 'required|exists:user,id',
          'task_id' => 'required|exists:task,id'
        ]);
    }
}
