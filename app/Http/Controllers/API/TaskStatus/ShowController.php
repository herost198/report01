<?php

namespace App\Http\Controllers\API\TaskStatus;

use App\Http\Controllers\Controller;
use App\Services\TaskStatusService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ShowController extends Controller
{
    protected $taskStatusService;

    public function __construct(TaskStatusService $taskStatusService)
    {
        $this->taskStatusService = $taskStatusService;
    }

    public function show($taskId)
    {
        $taskStatus = $this->taskStatusService->findByTaskID($taskId);
        return response()->json([
            'taskStatus' => $taskStatus,
        ], Response::HTTP_OK);
    }
}
