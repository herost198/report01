<?php

namespace App\Http\Controllers\API\TaskStatus;

use App\Enums\StatusTask;
use App\Enums\StatusTaskStatus;
use App\Http\Controllers\Controller;
use App\Services\FlowService;
use App\Services\TaskService;
use App\Services\TaskStatusService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UpdateController extends Controller
{
    protected $taskStatusService;
    protected $taskService;
    protected $flowService;

    public function __construct(
        TaskStatusService $taskStatusService,
        TaskService $taskService,
        FlowService $flowService
    ) {
        $this->taskStatusService = $taskStatusService;
        $this->taskService = $taskService;
        $this->flowService = $flowService;
    }

    public function main($taskStatusId, Request $request)
    {
        $statusTaskStatus = $request['status'];
        $acceptReview = $request['accept'];
        $statusTask = $request['status_task'];
        $taskStatusSelected = $this->taskStatusService->getByID($taskStatusId);
        $currentUser = Auth::user();
        if ($taskStatusSelected->user_id != $currentUser->id) {
            return response()->json([], 403);
        }
        $task = $this->taskService->getByID($taskStatusSelected->task_id);
        $flowsOfProject = $this->flowService->findFlowByProjectID($task->project_id);
        $numberOfFlows = count($flowsOfProject);
        DB::beginTransaction();
        try {
            if (!empty($acceptReview) && $acceptReview) {
                $statusTaskStatus = StatusTaskStatus::CLOSE;
            }
            switch ($task->status) {
                case StatusTask::OPEN:
                    if ($statusTaskStatus == StatusTaskStatus::INPROGESS) {
                        $this->taskService->updateByID($task->id, ['status' => StatusTask::ANNOTATING]);
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                case StatusTask::ANNOTATING:
                    if ($statusTaskStatus == StatusTaskStatus::CLOSE) {
                        $this->taskStatusService->updateByID(
                            $taskStatusId,
                            ['status' => $statusTaskStatus,
                            'is_active' => false]
                        );
                        $this->taskStatusService->updateByID($taskStatusId + 1, ['is_active' => true]);
                        $this->taskService->updateByID($task->id, ['status' => StatusTask::REVIEW_FIRST]);
                    } else {
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                case StatusTask::REVIEW_FIRST:
                    if ($statusTaskStatus == StatusTaskStatus::CLOSE) {
                        if ($numberOfFlows === 2) {
                            $this->taskService->updateByID($task->id, ['status' => StatusTask::FINISH]);
                        } else {
                            $this->taskStatusService->updateByID($taskStatusId + 1, ['is_active' => true]);
                            $this->taskService->updateByID($task->id, ['status' => StatusTask::REVIEW_SECOND]);
                        }
                        $this->taskStatusService->updateByID(
                            $taskStatusId,
                            ['status' => $statusTaskStatus,
                            'is_active' => false]
                        );
                    } else {
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                case StatusTask::REVIEW_SECOND:
                    if ($statusTaskStatus == StatusTaskStatus::CLOSE) {
                        if ($numberOfFlows === 3) {
                            $this->taskService->updateByID($task->id, ['status' => StatusTask::FINISH]);
                        } else {
                            $this->taskStatusService->updateByID($taskStatusId + 1, ['is_active' => true]);
                            $this->taskService->updateByID($task->id, ['status' => StatusTask::REVIEW_THIRD]);
                        }
                        $this->taskStatusService->updateByID(
                            $taskStatusId,
                            ['status' => $statusTaskStatus, 'is_active' => false]
                        );
                    } else {
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                case StatusTask::REVIEW_THIRD:
                    if ($statusTaskStatus == StatusTaskStatus::CLOSE) {
                        $this->taskService->updateByID($task->id, ['status' => StatusTask::FINISH]);
                        $this->taskStatusService->updateByID(
                            $taskStatusId,
                            ['status' => $statusTaskStatus,
                            'is_active' => false]
                        );
                    } else {
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                case StatusTask::FIXING:
                    if ($statusTaskStatus == StatusTaskStatus::CLOSE) {
                        $this->taskService->updateByID($task->id, ['status' => $statusTask]);
                        $this->taskStatusService->updateByID(
                            $taskStatusId,
                            ['status' => $statusTaskStatus,
                            'is_active' => false]
                        );
                    } else {
                        $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    }
                    break;
                default:
                    $this->taskStatusService->updateByID($taskStatusId, ['status' => $statusTaskStatus]);
                    break;
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([], 400);
        }
        return response()->json([
            "message" => "Change task status successfully!"
        ], 200);
    }
}
