<?php

namespace App\Http\Middleware;

use App\Enums\UserRole;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class CheckUserRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $userRole = Auth::user()->role;
        $stringUserRole = UserRole::getKey($userRole);
        foreach ($roles as $role) {
            if ($stringUserRole == $role) {
                return $next($request);
            }
        }
        return response()->json([
            'message' => config('constants.message_user.permission_error')
        ], Response::HTTP_FORBIDDEN);
    }
}
