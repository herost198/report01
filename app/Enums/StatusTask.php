<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatusTask extends Enum
{
    const UNASSIGN = 0;
    const OPEN = 1;
    const ANNOTATING = 2;
    const FIXING = 3;
    const REVIEW_FIRST = 4;
    const REVIEW_SECOND = 5;
    const REVIEW_THIRD = 6;
    const FINISH = 9;
}
