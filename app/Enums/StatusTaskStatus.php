<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class StatusTaskStatus extends Enum
{
    const OPEN = 1;
    const INPROGESS = 2;
    const CLOSE = 3;
}
