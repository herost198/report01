<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class UserStatus
 * @package App\Enums
 */
final class UserStatus extends Enum
{
    const INACTIVE =   9;
    const ACTIVE =   1;
}
