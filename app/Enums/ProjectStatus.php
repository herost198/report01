<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ProjectStatus extends Enum
{
    const OPEN = 1;
    const DOING = 2;
    const DONE = 3;
}
