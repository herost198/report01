<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Enums\UserRole;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();
        DB::table('user')->insert([
            'email' => 'admin@hblab.vn',
            'full_name' => "Admin",
            'phone' => '012345678',
            'password' => Hash::make('123456abc'),
            'role' => UserRole::ADMIN,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
