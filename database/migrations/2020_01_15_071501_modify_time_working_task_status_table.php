<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyTimeWorkingTaskStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('task_status', function (Blueprint $table) {
            $table->renameColumn('time_minute', 'time_working');
        });
        Schema::table('task_status', function (Blueprint $table) {
            $table->string('time_working')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('task_status', function (Blueprint $table) {
            $table->unsignedBigInteger('time_working')->change();
        });
        Schema::table('task_status', function (Blueprint $table) {
            $table->renameColumn('time_working', 'time_minute');
        });
    }
}
